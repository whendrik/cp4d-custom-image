# Example of adding an Custom Image (environment) for Watson Studio on CP4D 3

## Assumptions and Documentation

Here we follow the [Official Documentation on Building custom images](https://www.ibm.com/support/producthub/icpdata/docs/content/SSQNUZ_current/wsj/analyze-data/build-cust-images.html), and customize the image on the Open-Shift cluster itself.

Our goal is to customize the image;
- Install some libraries
- Exposing port `6006`, to access the tensorboard

## 1 - Preparing to build an image

[Downloading the configuration file](https://www.ibm.com/support/producthub/icpdata/docs/content/SSQNUZ_current/wsj/analyze-data/download-runtime-def.html) is straightforward.

e.g. to obtain a token, or use the expression with `sed` to directly save as ENV

```
myToken=`curl -k https://10.130.3.4:8443/v1/preauth/validateAuth -u admin:password | sed -n -e 's/^.*accessToken":"//p' | cut -d'"' -f1`
```
And download the json with
```
curl -X GET "https://10.130.3.4:8443/zen-data/v1/volumes/files/%2F_global_%2Fconfig%2F.runtime-definitions%2Fibm%2Fjupyter-py36-server.json" --header "Authorization: Bearer ${myToken}" -k -O
```

---

The image can be found with e.g. `cat jupyter-py36-server.json | grep image`

```
docker-registry.default.svc:5000/icp4d-test/wslocal-x86-runtime-py36ce162:cpd301-master-979
```

---

To [Download the associated image](https://www.ibm.com/support/producthub/icpdata/docs/content/SSQNUZ_current/wsj/analyze-data/download-base-image.html), find the registry with;

```
oc get route -n openshift-image-registry
```
or 
```
oc get route -n default
```
and login with e.g.


```
docker login docker-registry.default.svc:5000 -u $(oc whoami) -p $(oc whoami -t)
```

After login the image can be pulled with

```
docker pull docker-registry.default.svc:5000/icp4d-test/wslocal-x86-runtime-py36ce162:cpd301-master-979
```

and should appear with `docker images` command.

## 2 - Create a custom image

Create a `DOCKERFILE` based on the [example](https://www.ibm.com/support/producthub/icpdata/docs/content/SSQNUZ_current/wsj/analyze-data/customize-dockerfile.html)

- Add a line `EXPOSE 6006` if you want to [expose](https://docs.docker.com/engine/reference/builder/#expose) port 6006 for Tensorboard. 
- Optionally add `RUN ...` to install packages and/or load files
- Add the `DOCKERFILE` in an empty directory when executing `docker build`

## 3 - Register the custom image

[Registering](https://www.ibm.com/support/producthub/icpdata/docs/content/SSQNUZ_current/wsj/analyze-data/register-image.html) the custom image

## 4 - Customize and upload the configuration

Note the warning;

> **Important:** All runtime configuration file names must end with -server.json, including the configuration files you customize, for example `custom-runtime-def-1-server.json`.

Besides the suggested name modification, change the `portMappings`;

```
   "runtimeType": "jupyter-py36",
    "portMappings": [
      {
        "servicePort": 8888,
        "containerPort": 8888,
        "protocol": "TCP"
      },
      {
        "servicePort": 6006,
        "containerPort": 6006,
        "protocol": "TCP"
      },
      {
        "servicePort": 8889,
        "containerPort": 8889,
        "protocol": "TCP"
      }
    ],
    "replicas": 1,
    "image": "docker-registry.default.svc:5000/icp4d-test/wslocal-x86-runtime-py36ce162:cpd301-master-979",
    "command": ["/usr/sbin/tini", "--", "/opt/ibm/ws/bin/setup_container.sh"],
    "env": [
```

## 5 - Optionally, add a route

When the custom env is running, we should see the service with `oc get svc | grep jupyter`;
```
cdsx-jupyter-notebooks-converter-service                ClusterIP   172.30.210.192   <none>        443/TCP                                                                           19d
jupyter-py36-857094c7-f766-4d1a-9e78-428757fb9af5-svc   ClusterIP   172.30.115.23    <none>        8888/TCP,8889/TCP,6006/TCP                                                        7h
```
Indeed, `6006` is forwarded.

Notice we didn't actually start the tensorboard in this example.

(!) UNTESTED:

To access the tensorboard, either;

- `expose` the service, with oc `expose service jupyter-py36-857094c7-f766-4d1a-9e78-428757fb9af5-svc` or similar.

-or-

- Find the node ip with `oc describe svc jupyter-py36-857094c7-f766-4d1a-9e78-428757fb9af5-svc` or similar, and `ssh` with port forwarding